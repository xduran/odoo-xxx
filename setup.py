import os
import shutil
from setuptools import setup, find_packages
from distutils.sysconfig import get_python_lib
from subprocess import call

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

#copy src addons to the fixed location
wds_addons_path = os.path.join(get_python_lib(), 'soltein_oddo_addons/')

if not os.path.exists(wds_addons_path):
    os.mkdir(wds_addons_path)

for dir_name in os.listdir('src'):
    old_path = os.path.join('src/', dir_name)
    if os.path.isdir(old_path):
        new_path = os.path.join(wds_addons_path, dir_name)
        if os.path.exists(new_path):
            shutil.rmtree(new_path)
        shutil.copytree(old_path, new_path)


call("pip install -r reqs.txt", shell=True)

setup(
    name='odoo-xxx',
    version='1.2',
    packages=find_packages(),
)

